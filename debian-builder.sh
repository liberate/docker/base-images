#!/bin/bash
set -x

# bash-specific
set -o pipefail

env DEBIAN_FRONTEND=noninteractive apt install -y curl mmdebstrap dctrl-tools
# the parameters for the image

# where to fetch the packages from
# This is not necessary, its the default, and if you use the default
# the security mirror is setup automatically
#MIRROR="http://deb.debian.org/debian"

# If you specify a mirror manually, then you have to specify all of them manually
if [ ! -z "$ADD_MIRROR" ]
then
    cat <<EOF > mirror.list
$ADD_MIRROR
deb http://deb.debian.org/debian ${SUITE} main
deb http://deb.debian.org/debian ${SUITE}-updates main
deb http://security.debian.org/debian-security ${SUITE}-security main
EOF
fi

# this will really always be "debian", but might tolerate "ubuntu" if
# you (a) change the mirror above, (b) the suite, (c) the variant,
# and, more importantly, if mmdebootstrap supports it
IMAGE=debian
# The suite is controlled by the environment variable in .gitlab-ci.yml
VARIANT=minbase
RUNTIME=podman

# No servicable parts below this line
MODE=fakeroot
TAG=$CI_JOB_NAME-$VARIANT

# this creates an image similar to the official ones built by
# debuerreotype, except that instead of needing a whole set of scripts
# and hacks, we only rely on mmdebstrap.
#
# the downside is that the image is not reproducible (even if ran
# against the same mirror without change), mainly because `docker
# import` stores the import timestamp inside the image. however, the
# internal checksum itself should be reproducible.
mmdebstrap \
  --mode=$MODE \
  --variant=$VARIANT \
  --aptopt='Acquire::Languages "none"' \
  --dpkgopt='path-exclude=/usr/share/man/*' \
  --dpkgopt='path-exclude=/usr/share/man/man[1-9]/*' \
  --dpkgopt='path-exclude=/usr/share/locale/*' \
  --dpkgopt='path-include=/usr/share/locale/locale.alias' \
  --dpkgopt='path-exclude=/usr/share/lintian/*' \
  --dpkgopt='path-exclude=/usr/share/info/*' \
  --dpkgopt='path-exclude=/usr/share/doc/*' \
  --dpkgopt='path-include=/usr/share/doc/*/copyright' \
  --dpkgopt='path-exclude=/usr/share/{doc,info,man,omf,help,gnome/help,examples}/*' \
  --include='ca-certificates' \
  $SUITE \
  - \
  ${ADD_MIRROR:+mirror.list} |
    $RUNTIME import -c 'CMD ["bash"]' - $IMAGE:$TAG

$RUNTIME push $IMAGE:$TAG $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG

# note that we could also have used `| tar --delete /$PATH` to exclude files
