#!/bin/bash
set -euxo pipefail

# Use vfs with buildah. Docker offers overlayfs as a default, but buildah
# cannot stack overlayfs on top of another overlayfs filesystem.
export STORAGE_DRIVER=vfs

# Log into the container repository.
echo "$CI_REGISTRY_PASSWORD" | buildah login -u "$CI_REGISTRY_USER" --password-stdin $CI_REGISTRY

# Set up the container's fully qualified name.
FQ_IMAGE_NAME="${CI_REGISTRY_IMAGE}:${IMAGE_NAME}"

# Build the container, squash it, and push it to the repository.
buildah --storage-driver=vfs bud -f builds/${IMAGE_NAME} -t ${IMAGE_NAME} .
buildah --storage-driver=vfs push ${IMAGE_NAME} ${FQ_IMAGE_NAME}
